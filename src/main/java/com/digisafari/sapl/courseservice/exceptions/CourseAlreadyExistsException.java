package com.digisafari.sapl.courseservice.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.CONFLICT, reason = "Course already exists with the ID")
public class CourseAlreadyExistsException extends Exception {

}
