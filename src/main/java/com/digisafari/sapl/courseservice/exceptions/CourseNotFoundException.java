package com.digisafari.sapl.courseservice.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.NOT_FOUND, reason = "Course does not exist with this ID")
public class CourseNotFoundException extends Exception {

}
